# Pipewire Modifier Merging Test

## Description

This repository takes the video-src.c and video-play.c examples from pipewire to demonstrate the current behaviour on how formats and modifiers are negotiatied.

## Build

Run

```bash
PIPEWIRE_REPO_PATH=</Path/to/pipewire> ./build.sh
```

Pipewire has to be checked out and build with this patches included: https://gitlab.freedesktop.org/pipewire/pipewire/-/merge_requests/683

## Test

The simulated variants are:

### video-src

dmabuf:

* format: `SPA_VIDEO_FORMAT_RGB`, modifier: 3
* format: `SPA_VIDEO_FORMAT_RGBx`, modifier: 2
* format: `SPA_VIDEO_FORMAT_RGBA`, modifier: 3

shm:

* format: `SPA_VIDEO_FORMAT_RGB`
* format: `SPA_VIDEO_FORMAT_DSP_F32`

### video-src-shm-only

* format: `SPA_VIDEO_FORMAT_RGB`

### video-dmabuf-only

* format: `SPA_VIDEO_FORMAT_RGB`, modifier: 5
* format: `SPA_VIDEO_FORMAT_RGBx`, modifier: 2
* format: `SPA_VIDEO_FORMAT_RGBA`, modifier: 3

### video-src-wrongmodifier

dmabuf:

* format: `SPA_VIDEO_FORMAT_RGB`, modifier: 5

shm:

* format: `SPA_VIDEO_FORMAT_RGB`
* format: `SPA_VIDEO_FORMAT_DSP_F32`


### video-play

dmabuf:

* format: `SPA_VIDEO_FORMAT_RGB`, modifier: 4, 3
* format: `SPA_VIDEO_FORMAT_RGBx`, modifier: 2
* format: `SPA_VIDEO_FORMAT_RGBA`, modifier: 3

shm:

* format: `SPA_VIDEO_FORMAT_RGB`
* format: `SPA_VIDEO_FORMAT_DSP_F32`

### video-play-dmabuf-only

* format: `SPA_VIDEO_FORMAT_RGB`, modifier: 4, 3
* format: `SPA_VIDEO_FORMAT_RGBx`, modifier: 2
* format: `SPA_VIDEO_FORMAT_RGBA`, modifier: 3

### video-play-shm-only

shm:

* format: `SPA_VIDEO_FORMAT_RGB`
* format: `SPA_VIDEO_FORMAT_DSP_F32`

### Result

|                         | video-play | video-play-dmabuf-only | video-play-shm-only |
| ----------------------- | ---------- | ---------------------- | ------------------- |
| video-src               | s          | s                      | s                   |
| video-src-shm-only      | s          | s                      | s                   |
| video-src-dmabuf-only   | s          | s                      | s                   |
| video-src-wrongmodifier | s          | s                      | s                   |

s for success (works as expected), f for failure

* video-src -> video-play

  * format: `SPA_VIDEO_FORMAT_RGB`, modifier: 3

  This is a valid combination and works like expected.

* video-src-shm-only -> video-play

  * format: `SPA_VIDEO_FORMAT_RGB`

  This is a valid combination and works like expected.

* video-src-dmabuf-only -> video-play

  * format: `SPA_VIDEO_FORMAT_RGBx`, modifier 2

  This is a valid combination and works like expected.

* video-src-wrongmodifier-> video-play

  * format: `SPA_VIDEO_FORMAT_RGB`

  This falls back to shm format like expected.

* video-src -> video-play-dmabuf-only

  * format: `SPA_VIDEO_FORMAT_RGB`, modifier: 3

  This is a valid combination and works like expected.

* video-src-shm-only -> video-play-dmabuf-only

  * format: `NONE`, modifier: NONE

  This doesn't work like expected.

* video-src-dmabuf-only -> video-play-dmabuf-only

  * format: `SPA_VIDEO_FORMAT_RGBx`, modifier 2

  This is a valid combination and works like expected.

* video-src-wrongmodifier -> video-play-dmabuf-only

  * format: `NONE`, modifier: NONE

  This doesn't work like expected.

* video-src -> video-play-shm-only

  * format: `SPA_VIDEO_FORMAT_RGB`

  This is a valid combination and works like expected.

* video-src-shm-only -> video-play-shm-only

  * format: `SPA_VIDEO_FORMAT_RGB`

  This is a valid combination and works like expected.

* video-src-dmabuf-only -> video-play-shm-only

  * format: `NONE`, modifier NONE

  This doesn't work like expected.

* video-src-wrongmodifier -> video-play-shm-only

  * format: `SPA_VIDEO_FORMAT_RGB`

  This is a valid combination and works like expected.

Note:

Clients which support the old dmabuf api, which have not the posibility to query the supported modifiers for a format are expected to announce `DRM_FORMAT_MOD_INVALID` and are expected to work as shown above.

### Conclusion:

Looks like the SPA_POD_PROP_FLAG_MANDATORY is working as expected \o/
