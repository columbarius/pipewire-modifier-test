#!/bin/sh

pwflags=""
pw_flags() {
	#pwflags="$(pkg-config --cflags --libs libpipewire-0.3)"
	pwflags="-I/usr/include/pipewire-0.3 -I$PIPEWIRE_REPO_PATH/spa/include -D_REENTRANT -lpipewire-0.3"
}

pw_flags
gcc -Wall video-play.c -o video-play $pwflags $(pkg-config --cflags --libs sdl2) -lm
gcc -Wall video-play-shm-only.c -o video-play-shm-only $pwflags $(pkg-config --cflags --libs sdl2) -lm
gcc -Wall video-play-dmabuf-only.c -o video-play-dmabuf-only $pwflags $(pkg-config --cflags --libs sdl2) -lm
gcc -Wall video-src.c -o video-src $pwflags -lm
gcc -Wall video-src-shm-only.c -o video-src-shm-only $pwflags -lm
gcc -Wall video-src-dmabuf-only.c -o video-src-dmabuf-only $pwflags -lm
gcc -Wall video-src-wrongmodifier.c -o video-src-wrongmodifier $pwflags -lm
