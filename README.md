# Pipewire Modifier Test

## Description

This repository takes the video-src.c and video-play.c examples from pipewire to demonstrate the current behaviour on how formats and modifiers are negotiatied.

## Tests

* [Merging of modifier lists](merging/README.md)
